﻿using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour {

	public string name;
	public int health;
	public Material material;
	public bool ranged;
	public bool close;
	public int agro;
	public bool flying;
	
	public int initiative;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
