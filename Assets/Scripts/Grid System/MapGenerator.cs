﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MapGenerator : MonoBehaviour {
	public class Map{
		public int size;
		public int dificulty;
		public int wetness;
		public bool traps = false;
		public List<Room> rooms = new List<Room>();

		public Map(int size)
		{
			for (int i = 0; i < size; i++) {
				// Room(int h_size, int v_size, int room_number, int monsters
				rooms.Add( new Room(15, 15, i, 3));
			}
		}
	}

	public class MonsterSpawn{
		public int h_pos;
		public int v_pos;
		public int difficulty;
		public bool ranged;
		public Material material;

		public MonsterSpawn(int newh_pos, int newv_pos){
			h_pos= newh_pos;
			v_pos =newv_pos;
		}

	}

	public class Room{
		public int h_size;
		public int v_size;
		public int room_number;
		public List<Tile> tiles = new List<Tile>();
		public int monsters;

		public List<MonsterSpawn> mon_tiles = new List<MonsterSpawn>();

		public Room(int h_size, int v_size, int room_number, int monsters){

			tiles = tiles;

			for (int i = 0; i < monsters; i++) {
				int h = Random.Range(1, h_size);
				int v = Random.Range(1, v_size);

				mon_tiles.Add (new MonsterSpawn(h,v));
			}

			string tile_resource = "LightStone";

			for (int row = 0; row < v_size; row++) {
				for (int col = 0; col < h_size; col++) {

					if (tile_resource == "LightStone"){
						tile_resource = "DarkStone";
					}else{
						tile_resource = "LightStone";
					}
					Debug.Log(col);
					tiles.Add( new Tile(col, row, tile_resource));
				}
			}
		}
	
	}

	public class Tile
	{
		public int h_pos;
		public int v_pos;
		public Material material;
		public bool torch;
		public bool player_spawn;
		public bool monster_spawn;
		public int portal;
		
		public string TileType;

		public Tile (int newh_pos, int newv_pos, string newTileType){
			TileType = newTileType;
			h_pos = newh_pos;
			v_pos = newv_pos;


		}

		
	}



	// Use this for initialization
	void Start () {
		//GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		//cube.transform.position = new Vector3(0, 0.5F, 0);
		Map MyMap = new Map(1);

		//Debug.Log ("FAAAAART: ");
		//Debug.Log (MyMap);
		Debug.Log (MyMap.rooms[0].tiles);

		foreach (Tile i in MyMap.rooms[0].tiles){
			Debug.Log (i.TileType);
			GameObject instance = Instantiate (Resources.Load (i.TileType, typeof(GameObject))) as GameObject;
			instance.transform.position = new Vector3(0+i.h_pos, 0, 0+i.v_pos);
		}

		foreach (MonsterSpawn i in MyMap.rooms[0].mon_tiles){
			GameObject instance = Instantiate (Resources.Load ("Monster", typeof(GameObject))) as GameObject;
			instance.transform.position = new Vector3(0+i.h_pos, 1, 0+i.v_pos);
		}

		//Debug.Log (MyMap.size);
		/*
		for (int row = 0; row < v_size; row++) {
			for (int i = 0; i < h_size; i++) {
				if (tile_resource == "LightStone"){
					tile_resource = "DarkStone";
				}else{
					tile_resource = "LightStone";
				}
				GameObject instance = Instantiate (Resources.Load (tile_resource, typeof(GameObject))) as GameObject;
				instance.transform.position = new Vector3(0+i, 0, 0+row);
			}
		}
		*/
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

